

start: 0 midiStart: 0 pitch: 72 measure: 1 duration: 4

start: 4 midiStart: 240 pitch: 71 measure: 2 duration: 2
start: 4 midiStart: 240 pitch: 45 measure: 2 duration: 8

start: 6 midiStart: 360 pitch: 72 measure: 2 duration: 2
start: 4 midiStart: 240 pitch: 45 measure: 2 duration: 8

start: 8 midiStart: 480 pitch: 74 measure: 2 duration: 8
start: 4 midiStart: 240 pitch: 45 measure: 2 duration: 8

start: 8 midiStart: 480 pitch: 74 measure: 2 duration: 8
start: 12 midiStart: 720 pitch: 57 measure: 2 duration: 8
start: 12 midiStart: 720 pitch: 60 measure: 2 duration: 8
start: 12 midiStart: 720 pitch: 64 measure: 2 duration: 8

start: 16 midiStart: 960 pitch: 72 measure: 2 duration: 4
start: 12 midiStart: 720 pitch: 57 measure: 2 duration: 8
start: 12 midiStart: 720 pitch: 60 measure: 2 duration: 8
start: 12 midiStart: 720 pitch: 64 measure: 2 duration: 8

start: 20 midiStart: 1200 pitch: 74 measure: 3 duration: 2
start: 20 midiStart: 1200 pitch: 38 measure: 3 duration: 8

start: 22 midiStart: 1320 pitch: 76 measure: 3 duration: 2
start: 20 midiStart: 1200 pitch: 38 measure: 3 duration: 8

start: 24 midiStart: 1440 pitch: 76 measure: 3 duration: 8
start: 20 midiStart: 1200 pitch: 38 measure: 3 duration: 8

start: 24 midiStart: 1440 pitch: 76 measure: 3 duration: 8
start: 28 midiStart: 1680 pitch: 57 measure: 3 duration: 8
start: 28 midiStart: 1680 pitch: 60 measure: 3 duration: 8
start: 28 midiStart: 1680 pitch: 62 measure: 3 duration: 8

start: 32 midiStart: 1920 pitch: 78 measure: 3 duration: 4
start: 28 midiStart: 1680 pitch: 57 measure: 3 duration: 8
start: 28 midiStart: 1680 pitch: 60 measure: 3 duration: 8
start: 28 midiStart: 1680 pitch: 62 measure: 3 duration: 8

start: 36 midiStart: 2160 pitch: 78 measure: 4 duration: 2
start: 36 midiStart: 2160 pitch: 43 measure: 4 duration: 8

start: 38 midiStart: 2280 pitch: 79 measure: 4 duration: 2
start: 36 midiStart: 2160 pitch: 43 measure: 4 duration: 8

start: 40 midiStart: 2400 pitch: 79 measure: 4 duration: 8
start: 36 midiStart: 2160 pitch: 43 measure: 4 duration: 8

start: 40 midiStart: 2400 pitch: 79 measure: 4 duration: 8
start: 44 midiStart: 2640 pitch: 60 measure: 4 duration: 4

start: 48 midiStart: 2880 pitch: 77 measure: 4 duration: 2
start: 48 midiStart: 2880 pitch: 59 measure: 4 duration: 4

start: 50 midiStart: 3000 pitch: 76 measure: 4 duration: 2
start: 48 midiStart: 2880 pitch: 59 measure: 4 duration: 4

start: 52 midiStart: 3120 pitch: 76 measure: 5 duration: 2
start: 52 midiStart: 3120 pitch: 59 measure: 5 duration: 8
start: 52 midiStart: 3120 pitch: 60 measure: 5 duration: 8

start: 54 midiStart: 3240 pitch: 74 measure: 5 duration: 2
start: 52 midiStart: 3120 pitch: 59 measure: 5 duration: 8
start: 52 midiStart: 3120 pitch: 60 measure: 5 duration: 8

start: 56 midiStart: 3360 pitch: 72 measure: 5 duration: 4
start: 52 midiStart: 3120 pitch: 59 measure: 5 duration: 8
start: 52 midiStart: 3120 pitch: 60 measure: 5 duration: 8

start: 60 midiStart: 3600 pitch: 72 measure: 5 duration: 4
start: 60 midiStart: 3600 pitch: 57 measure: 5 duration: 4


start: 68 midiStart: 4080 pitch: 71 measure: 6 duration: 2
start: 68 midiStart: 4080 pitch: 40 measure: 6 duration: 8

start: 70 midiStart: 4200 pitch: 72 measure: 6 duration: 2
start: 68 midiStart: 4080 pitch: 40 measure: 6 duration: 8

start: 72 midiStart: 4320 pitch: 74 measure: 6 duration: 8
start: 68 midiStart: 4080 pitch: 40 measure: 6 duration: 8

start: 72 midiStart: 4320 pitch: 74 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 52 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 55 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 59 measure: 6 duration: 8

start: 80 midiStart: 4800 pitch: 74 measure: 6 duration: 2
start: 76 midiStart: 4560 pitch: 52 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 55 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 59 measure: 6 duration: 8

start: 82 midiStart: 4920 pitch: 71 measure: 6 duration: 1
start: 76 midiStart: 4560 pitch: 52 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 55 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 59 measure: 6 duration: 8

start: 83 midiStart: 4980 pitch: 74 measure: 6 duration: 1
start: 76 midiStart: 4560 pitch: 52 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 55 measure: 6 duration: 8
start: 76 midiStart: 4560 pitch: 59 measure: 6 duration: 8

start: 84 midiStart: 5040 pitch: 72 measure: 7 duration: 7
start: 84 midiStart: 5040 pitch: 52 measure: 7 duration: 16
start: 84 midiStart: 5040 pitch: 57 measure: 7 duration: 8
start: 84 midiStart: 5040 pitch: 60 measure: 7 duration: 8

start: 91 midiStart: 5460 pitch: 69 measure: 7 duration: 1
start: 84 midiStart: 5040 pitch: 52 measure: 7 duration: 16
start: 84 midiStart: 5040 pitch: 57 measure: 7 duration: 8
start: 84 midiStart: 5040 pitch: 60 measure: 7 duration: 8

start: 92 midiStart: 5520 pitch: 71 measure: 7 duration: 4
start: 84 midiStart: 5040 pitch: 52 measure: 7 duration: 16
start: 92 midiStart: 5520 pitch: 59 measure: 7 duration: 8
start: 92 midiStart: 5520 pitch: 62 measure: 7 duration: 8

start: 96 midiStart: 5760 pitch: 67 measure: 7 duration: 4
start: 84 midiStart: 5040 pitch: 52 measure: 7 duration: 16
start: 92 midiStart: 5520 pitch: 59 measure: 7 duration: 8
start: 92 midiStart: 5520 pitch: 62 measure: 7 duration: 8

start: 100 midiStart: 6000 pitch: 69 measure: 8 duration: 16
start: 100 midiStart: 6000 pitch: 57 measure: 8 duration: 8
start: 100 midiStart: 6000 pitch: 61 measure: 8 duration: 8
start: 100 midiStart: 6000 pitch: 64 measure: 8 duration: 8

start: 100 midiStart: 6000 pitch: 69 measure: 8 duration: 16
start: 108 midiStart: 6480 pitch: 45 measure: 8 duration: 8

start: 116 midiStart: 6960 pitch: 57 measure: 9 duration: 14
start: 116 midiStart: 6960 pitch: 61 measure: 9 duration: 8
start: 116 midiStart: 6960 pitch: 64 measure: 9 duration: 8

start: 116 midiStart: 6960 pitch: 57 measure: 9 duration: 14
start: 124 midiStart: 7440 pitch: 33 measure: 9 duration: 8

start: 130 midiStart: 7800 pitch: 72 measure: 9 duration: 2
start: 124 midiStart: 7440 pitch: 33 measure: 9 duration: 8

start: 132 midiStart: 7920 pitch: 71 measure: 10 duration: 2
start: 132 midiStart: 7920 pitch: 57 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 62 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 66 measure: 10 duration: 8

start: 134 midiStart: 8040 pitch: 72 measure: 10 duration: 2
start: 132 midiStart: 7920 pitch: 57 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 62 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 66 measure: 10 duration: 8

start: 136 midiStart: 8160 pitch: 74 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 57 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 62 measure: 10 duration: 8
start: 132 midiStart: 7920 pitch: 66 measure: 10 duration: 8

start: 136 midiStart: 8160 pitch: 74 measure: 10 duration: 8
start: 140 midiStart: 8400 pitch: 45 measure: 10 duration: 8

start: 144 midiStart: 8640 pitch: 72 measure: 10 duration: 4
start: 140 midiStart: 8400 pitch: 45 measure: 10 duration: 8

start: 148 midiStart: 8880 pitch: 74 measure: 11 duration: 2
start: 148 midiStart: 8880 pitch: 57 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 60 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 64 measure: 11 duration: 8

start: 150 midiStart: 9000 pitch: 76 measure: 11 duration: 2
start: 148 midiStart: 8880 pitch: 57 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 60 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 64 measure: 11 duration: 8

start: 152 midiStart: 9120 pitch: 76 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 57 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 60 measure: 11 duration: 8
start: 148 midiStart: 8880 pitch: 64 measure: 11 duration: 8

start: 152 midiStart: 9120 pitch: 76 measure: 11 duration: 8
start: 156 midiStart: 9360 pitch: 62 measure: 11 duration: 8

start: 160 midiStart: 9600 pitch: 78 measure: 11 duration: 4
start: 156 midiStart: 9360 pitch: 62 measure: 11 duration: 8

start: 164 midiStart: 9840 pitch: 78 measure: 12 duration: 2
start: 164 midiStart: 9840 pitch: 60 measure: 12 duration: 8
start: 164 midiStart: 9840 pitch: 62 measure: 12 duration: 8

start: 166 midiStart: 9960 pitch: 79 measure: 12 duration: 2
start: 164 midiStart: 9840 pitch: 60 measure: 12 duration: 8
start: 164 midiStart: 9840 pitch: 62 measure: 12 duration: 8

start: 168 midiStart: 10080 pitch: 79 measure: 12 duration: 8
start: 164 midiStart: 9840 pitch: 60 measure: 12 duration: 8
start: 164 midiStart: 9840 pitch: 62 measure: 12 duration: 8

start: 168 midiStart: 10080 pitch: 79 measure: 12 duration: 8
start: 172 midiStart: 10320 pitch: 59 measure: 12 duration: 8

start: 176 midiStart: 10560 pitch: 77 measure: 12 duration: 2
start: 172 midiStart: 10320 pitch: 59 measure: 12 duration: 8

start: 178 midiStart: 10680 pitch: 76 measure: 12 duration: 2
start: 172 midiStart: 10320 pitch: 59 measure: 12 duration: 8

start: 180 midiStart: 10800 pitch: 76 measure: 13 duration: 2
start: 180 midiStart: 10800 pitch: 55 measure: 13 duration: 8
start: 180 midiStart: 10800 pitch: 64 measure: 13 duration: 8
start: 180 midiStart: 10800 pitch: 57 measure: 13 duration: 2

start: 182 midiStart: 10920 pitch: 74 measure: 13 duration: 2
start: 180 midiStart: 10800 pitch: 55 measure: 13 duration: 8
start: 180 midiStart: 10800 pitch: 64 measure: 13 duration: 8
start: 182 midiStart: 10920 pitch: 59 measure: 13 duration: 2

start: 184 midiStart: 11040 pitch: 72 measure: 13 duration: 4
start: 180 midiStart: 10800 pitch: 55 measure: 13 duration: 8
start: 180 midiStart: 10800 pitch: 64 measure: 13 duration: 8
start: 184 midiStart: 11040 pitch: 60 measure: 13 duration: 4

start: 188 midiStart: 11280 pitch: 72 measure: 13 duration: 4
start: 188 midiStart: 11280 pitch: 36 measure: 13 duration: 4

start: 192 midiStart: 11520 pitch: 38 measure: 13 duration: 4

start: 196 midiStart: 11760 pitch: 71 measure: 14 duration: 2
start: 196 midiStart: 11760 pitch: 40 measure: 14 duration: 8

start: 198 midiStart: 11880 pitch: 72 measure: 14 duration: 2
start: 196 midiStart: 11760 pitch: 40 measure: 14 duration: 8

start: 200 midiStart: 12000 pitch: 74 measure: 14 duration: 4
start: 196 midiStart: 11760 pitch: 40 measure: 14 duration: 8

start: 204 midiStart: 12240 pitch: 74 measure: 14 duration: 4
start: 204 midiStart: 12240 pitch: 52 measure: 14 duration: 8
start: 204 midiStart: 12240 pitch: 55 measure: 14 duration: 8
start: 204 midiStart: 12240 pitch: 59 measure: 14 duration: 8

start: 208 midiStart: 12480 pitch: 71 measure: 14 duration: 4
start: 204 midiStart: 12240 pitch: 52 measure: 14 duration: 8
start: 204 midiStart: 12240 pitch: 55 measure: 14 duration: 8
start: 204 midiStart: 12240 pitch: 59 measure: 14 duration: 8

start: 212 midiStart: 12720 pitch: 72 measure: 15 duration: 4
start: 212 midiStart: 12720 pitch: 52 measure: 15 duration: 16
start: 212 midiStart: 12720 pitch: 57 measure: 15 duration: 8
start: 212 midiStart: 12720 pitch: 60 measure: 15 duration: 8

start: 216 midiStart: 12960 pitch: 69 measure: 15 duration: 4
start: 212 midiStart: 12720 pitch: 52 measure: 15 duration: 16
start: 212 midiStart: 12720 pitch: 57 measure: 15 duration: 8
start: 212 midiStart: 12720 pitch: 60 measure: 15 duration: 8

start: 220 midiStart: 13200 pitch: 71 measure: 15 duration: 4
start: 212 midiStart: 12720 pitch: 52 measure: 15 duration: 16
start: 220 midiStart: 13200 pitch: 59 measure: 15 duration: 8
start: 220 midiStart: 13200 pitch: 62 measure: 15 duration: 8

start: 224 midiStart: 13440 pitch: 67 measure: 15 duration: 4
start: 212 midiStart: 12720 pitch: 52 measure: 15 duration: 16
start: 220 midiStart: 13200 pitch: 59 measure: 15 duration: 8
start: 220 midiStart: 13200 pitch: 62 measure: 15 duration: 8

start: 228 midiStart: 13680 pitch: 69 measure: 16 duration: 16
start: 228 midiStart: 13680 pitch: 57 measure: 16 duration: 8
start: 228 midiStart: 13680 pitch: 61 measure: 16 duration: 8
start: 228 midiStart: 13680 pitch: 64 measure: 16 duration: 8

start: 228 midiStart: 13680 pitch: 69 measure: 16 duration: 16
start: 236 midiStart: 14160 pitch: 45 measure: 16 duration: 8

start: 244 midiStart: 14640 pitch: 57 measure: 17 duration: 14
start: 244 midiStart: 14640 pitch: 61 measure: 17 duration: 8
start: 244 midiStart: 14640 pitch: 64 measure: 17 duration: 8

start: 244 midiStart: 14640 pitch: 57 measure: 17 duration: 14
start: 252 midiStart: 15120 pitch: 33 measure: 17 duration: 8

start: 258 midiStart: 15480 pitch: 67 measure: 17 duration: 2
start: 252 midiStart: 15120 pitch: 33 measure: 17 duration: 8

start: 260 midiStart: 15600 pitch: 69 measure: 18 duration: 12

start: 260 midiStart: 15600 pitch: 69 measure: 18 duration: 12
start: 268 midiStart: 16080 pitch: 57 measure: 18 duration: 8
start: 268 midiStart: 16080 pitch: 62 measure: 18 duration: 8
start: 268 midiStart: 16080 pitch: 66 measure: 18 duration: 8

start: 272 midiStart: 16320 pitch: 72 measure: 18 duration: 2
start: 268 midiStart: 16080 pitch: 57 measure: 18 duration: 8
start: 268 midiStart: 16080 pitch: 62 measure: 18 duration: 8
start: 268 midiStart: 16080 pitch: 66 measure: 18 duration: 8

start: 274 midiStart: 16440 pitch: 71 measure: 18 duration: 2
start: 268 midiStart: 16080 pitch: 57 measure: 18 duration: 8
start: 268 midiStart: 16080 pitch: 62 measure: 18 duration: 8
start: 268 midiStart: 16080 pitch: 66 measure: 18 duration: 8

start: 276 midiStart: 16560 pitch: 72 measure: 18 duration: 2

start: 278 midiStart: 16680 pitch: 74 measure: 19 duration: 6
start: 278 midiStart: 16680 pitch: 55 measure: 19 duration: 8
start: 278 midiStart: 16680 pitch: 60 measure: 19 duration: 8
start: 278 midiStart: 16680 pitch: 64 measure: 19 duration: 8

start: 284 midiStart: 17040 pitch: 69 measure: 19 duration: 2
start: 278 midiStart: 16680 pitch: 55 measure: 19 duration: 8
start: 278 midiStart: 16680 pitch: 60 measure: 19 duration: 8
start: 278 midiStart: 16680 pitch: 64 measure: 19 duration: 8

start: 286 midiStart: 17160 pitch: 74 measure: 19 duration: 4
start: 286 midiStart: 17160 pitch: 54 measure: 19 duration: 8
start: 286 midiStart: 17160 pitch: 59 measure: 19 duration: 8
start: 286 midiStart: 17160 pitch: 62 measure: 19 duration: 8

start: 290 midiStart: 17400 pitch: 71 measure: 19 duration: 4
start: 286 midiStart: 17160 pitch: 54 measure: 19 duration: 8
start: 286 midiStart: 17160 pitch: 59 measure: 19 duration: 8
start: 286 midiStart: 17160 pitch: 62 measure: 19 duration: 8

start: 294 midiStart: 17640 pitch: 72 measure: 20 duration: 12
start: 294 midiStart: 17640 pitch: 52 measure: 20 duration: 8
start: 294 midiStart: 17640 pitch: 59 measure: 20 duration: 8
start: 294 midiStart: 17640 pitch: 60 measure: 20 duration: 8

start: 294 midiStart: 17640 pitch: 72 measure: 20 duration: 12
start: 302 midiStart: 18120 pitch: 33 measure: 20 duration: 8

start: 306 midiStart: 18360 pitch: 71 measure: 20 duration: 4
start: 302 midiStart: 18120 pitch: 33 measure: 20 duration: 8

start: 310 midiStart: 18600 pitch: 72 measure: 21 duration: 4
start: 310 midiStart: 18600 pitch: 52 measure: 21 duration: 8
start: 310 midiStart: 18600 pitch: 57 measure: 21 duration: 8
start: 310 midiStart: 18600 pitch: 60 measure: 21 duration: 8

start: 310 midiStart: 18600 pitch: 52 measure: 21 duration: 8
start: 310 midiStart: 18600 pitch: 57 measure: 21 duration: 8
start: 310 midiStart: 18600 pitch: 60 measure: 21 duration: 8

start: 316 midiStart: 18960 pitch: 76 measure: 21 duration: 2
start: 310 midiStart: 18600 pitch: 52 measure: 21 duration: 8
start: 310 midiStart: 18600 pitch: 57 measure: 21 duration: 8
start: 310 midiStart: 18600 pitch: 60 measure: 21 duration: 8

start: 318 midiStart: 19080 pitch: 81 measure: 21 duration: 4
start: 318 midiStart: 19080 pitch: 45 measure: 21 duration: 8
start: 318 midiStart: 19080 pitch: 55 measure: 21 duration: 8

start: 322 midiStart: 19320 pitch: 83 measure: 21 duration: 4
start: 318 midiStart: 19080 pitch: 45 measure: 21 duration: 8
start: 318 midiStart: 19080 pitch: 55 measure: 21 duration: 8

start: 326 midiStart: 19560 pitch: 84 measure: 22 duration: 4
start: 326 midiStart: 19560 pitch: 53 measure: 22 duration: 8
start: 326 midiStart: 19560 pitch: 57 measure: 22 duration: 8
start: 326 midiStart: 19560 pitch: 64 measure: 22 duration: 8

start: 326 midiStart: 19560 pitch: 53 measure: 22 duration: 8
start: 326 midiStart: 19560 pitch: 57 measure: 22 duration: 8
start: 326 midiStart: 19560 pitch: 64 measure: 22 duration: 8

start: 332 midiStart: 19920 pitch: 83 measure: 22 duration: 2
start: 326 midiStart: 19560 pitch: 53 measure: 22 duration: 8
start: 326 midiStart: 19560 pitch: 57 measure: 22 duration: 8
start: 326 midiStart: 19560 pitch: 64 measure: 22 duration: 8

start: 334 midiStart: 20040 pitch: 81 measure: 22 duration: 4
start: 334 midiStart: 20040 pitch: 38 measure: 22 duration: 8

start: 338 midiStart: 20280 pitch: 79 measure: 22 duration: 4
start: 334 midiStart: 20040 pitch: 38 measure: 22 duration: 8

start: 342 midiStart: 20520 pitch: 77 measure: 23 duration: 4
start: 342 midiStart: 20520 pitch: 55 measure: 23 duration: 8
start: 342 midiStart: 20520 pitch: 59 measure: 23 duration: 8
start: 342 midiStart: 20520 pitch: 62 measure: 23 duration: 8

start: 342 midiStart: 20520 pitch: 55 measure: 23 duration: 8
start: 342 midiStart: 20520 pitch: 59 measure: 23 duration: 8
start: 342 midiStart: 20520 pitch: 62 measure: 23 duration: 8

start: 348 midiStart: 20880 pitch: 76 measure: 23 duration: 2
start: 342 midiStart: 20520 pitch: 55 measure: 23 duration: 8
start: 342 midiStart: 20520 pitch: 59 measure: 23 duration: 8
start: 342 midiStart: 20520 pitch: 62 measure: 23 duration: 8

start: 350 midiStart: 21000 pitch: 74 measure: 23 duration: 4
start: 350 midiStart: 21000 pitch: 40 measure: 23 duration: 8

start: 354 midiStart: 21240 pitch: 71 measure: 23 duration: 4
start: 350 midiStart: 21000 pitch: 40 measure: 23 duration: 8

start: 358 midiStart: 21480 pitch: 74 measure: 24 duration: 2
start: 358 midiStart: 21480 pitch: 52 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 59 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 60 measure: 24 duration: 8

start: 360 midiStart: 21600 pitch: 72 measure: 24 duration: 2
start: 358 midiStart: 21480 pitch: 52 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 59 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 60 measure: 24 duration: 8

start: 362 midiStart: 21720 pitch: 72 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 52 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 59 measure: 24 duration: 8
start: 358 midiStart: 21480 pitch: 60 measure: 24 duration: 8

start: 362 midiStart: 21720 pitch: 72 measure: 24 duration: 8
start: 366 midiStart: 21960 pitch: 45 measure: 24 duration: 8

start: 370 midiStart: 22200 pitch: 71 measure: 24 duration: 4
start: 366 midiStart: 21960 pitch: 45 measure: 24 duration: 8

start: 374 midiStart: 22440 pitch: 72 measure: 25 duration: 6
start: 374 midiStart: 22440 pitch: 52 measure: 25 duration: 8
start: 374 midiStart: 22440 pitch: 57 measure: 25 duration: 8
start: 374 midiStart: 22440 pitch: 60 measure: 25 duration: 8

start: 380 midiStart: 22800 pitch: 71 measure: 25 duration: 2
start: 374 midiStart: 22440 pitch: 52 measure: 25 duration: 8
start: 374 midiStart: 22440 pitch: 57 measure: 25 duration: 8
start: 374 midiStart: 22440 pitch: 60 measure: 25 duration: 8

start: 382 midiStart: 22920 pitch: 72 measure: 25 duration: 4
start: 382 midiStart: 22920 pitch: 43 measure: 25 duration: 8

start: 386 midiStart: 23160 pitch: 67 measure: 25 duration: 4
start: 382 midiStart: 22920 pitch: 43 measure: 25 duration: 8

start: 390 midiStart: 23400 pitch: 69 measure: 26 duration: 12
start: 390 midiStart: 23400 pitch: 41 measure: 26 duration: 8

start: 390 midiStart: 23400 pitch: 69 measure: 26 duration: 12
start: 398 midiStart: 23880 pitch: 57 measure: 26 duration: 8
start: 398 midiStart: 23880 pitch: 63 measure: 26 duration: 8

start: 402 midiStart: 24120 pitch: 72 measure: 26 duration: 2
start: 398 midiStart: 23880 pitch: 57 measure: 26 duration: 8
start: 398 midiStart: 23880 pitch: 63 measure: 26 duration: 8

start: 404 midiStart: 24240 pitch: 71 measure: 26 duration: 2
start: 398 midiStart: 23880 pitch: 57 measure: 26 duration: 8
start: 398 midiStart: 23880 pitch: 63 measure: 26 duration: 8

start: 406 midiStart: 24360 pitch: 72 measure: 26 duration: 2
start: 406 midiStart: 24360 pitch: 57 measure: 26 duration: 2
start: 406 midiStart: 24360 pitch: 63 measure: 26 duration: 2

start: 408 midiStart: 24480 pitch: 74 measure: 27 duration: 6
start: 408 midiStart: 24480 pitch: 57 measure: 27 duration: 8
start: 408 midiStart: 24480 pitch: 63 measure: 27 duration: 8

start: 414 midiStart: 24840 pitch: 69 measure: 27 duration: 2
start: 408 midiStart: 24480 pitch: 57 measure: 27 duration: 8
start: 408 midiStart: 24480 pitch: 63 measure: 27 duration: 8

start: 416 midiStart: 24960 pitch: 74 measure: 27 duration: 4
start: 416 midiStart: 24960 pitch: 64 measure: 27 duration: 8

start: 420 midiStart: 25200 pitch: 71 measure: 27 duration: 4
start: 416 midiStart: 24960 pitch: 64 measure: 27 duration: 8

start: 424 midiStart: 25440 pitch: 74 measure: 28 duration: 2
start: 424 midiStart: 25440 pitch: 38 measure: 28 duration: 8

start: 426 midiStart: 25560 pitch: 72 measure: 28 duration: 2
start: 424 midiStart: 25440 pitch: 38 measure: 28 duration: 8

start: 428 midiStart: 25680 pitch: 72 measure: 28 duration: 8
start: 424 midiStart: 25440 pitch: 38 measure: 28 duration: 8

start: 428 midiStart: 25680 pitch: 72 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 57 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 60 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 66 measure: 28 duration: 8

start: 436 midiStart: 26160 pitch: 74 measure: 28 duration: 2
start: 432 midiStart: 25920 pitch: 57 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 60 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 66 measure: 28 duration: 8

start: 438 midiStart: 26280 pitch: 72 measure: 28 duration: 2
start: 432 midiStart: 25920 pitch: 57 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 60 measure: 28 duration: 8
start: 432 midiStart: 25920 pitch: 66 measure: 28 duration: 8

start: 440 midiStart: 26400 pitch: 74 measure: 28 duration: 2
start: 440 midiStart: 26400 pitch: 57 measure: 28 duration: 2
start: 440 midiStart: 26400 pitch: 60 measure: 28 duration: 2
start: 440 midiStart: 26400 pitch: 66 measure: 28 duration: 2

start: 442 midiStart: 26520 pitch: 76 measure: 29 duration: 6
start: 442 midiStart: 26520 pitch: 57 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 60 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 66 measure: 29 duration: 16

start: 448 midiStart: 26880 pitch: 72 measure: 29 duration: 2
start: 442 midiStart: 26520 pitch: 57 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 60 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 66 measure: 29 duration: 16

start: 450 midiStart: 27000 pitch: 67 measure: 29 duration: 4
start: 442 midiStart: 26520 pitch: 57 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 60 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 66 measure: 29 duration: 16

start: 442 midiStart: 26520 pitch: 57 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 60 measure: 29 duration: 16
start: 442 midiStart: 26520 pitch: 66 measure: 29 duration: 16

start: 458 midiStart: 27480 pitch: 69 measure: 30 duration: 4

start: 462 midiStart: 27720 pitch: 76 measure: 30 duration: 4

start: 466 midiStart: 27960 pitch: 69 measure: 30 duration: 4
start: 466 midiStart: 27960 pitch: 50 measure: 30 duration: 8
start: 466 midiStart: 27960 pitch: 54 measure: 30 duration: 8
start: 466 midiStart: 27960 pitch: 60 measure: 30 duration: 8

start: 470 midiStart: 28200 pitch: 76 measure: 30 duration: 4
start: 466 midiStart: 27960 pitch: 50 measure: 30 duration: 8
start: 466 midiStart: 27960 pitch: 54 measure: 30 duration: 8
start: 466 midiStart: 27960 pitch: 60 measure: 30 duration: 8

start: 474 midiStart: 28440 pitch: 69 measure: 31 duration: 4
start: 474 midiStart: 28440 pitch: 57 measure: 31 duration: 8
start: 474 midiStart: 28440 pitch: 60 measure: 31 duration: 8
start: 474 midiStart: 28440 pitch: 66 measure: 31 duration: 8

start: 478 midiStart: 28680 pitch: 76 measure: 31 duration: 4
start: 474 midiStart: 28440 pitch: 57 measure: 31 duration: 8
start: 474 midiStart: 28440 pitch: 60 measure: 31 duration: 8
start: 474 midiStart: 28440 p