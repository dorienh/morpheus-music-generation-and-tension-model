

start: 0 midiStart: 0 pitch: 48 measure: 1 duration: 6

start: 3 midiStart: 120 pitch: 64 measure: 1 duration: 3
start: 3 midiStart: 120 pitch: 67 measure: 1 duration: 3
start: 3 midiStart: 120 pitch: 72 measure: 1 duration: 3
start: 0 midiStart: 0 pitch: 48 measure: 1 duration: 6

start: 6 midiStart: 240 pitch: 52 measure: 1 duration: 6

start: 9 midiStart: 360 pitch: 64 measure: 1 duration: 3
start: 9 midiStart: 360 pitch: 67 measure: 1 duration: 3
start: 9 midiStart: 360 pitch: 72 measure: 1 duration: 3
start: 6 midiStart: 240 pitch: 52 measure: 1 duration: 6

start: 12 midiStart: 480 pitch: 55 measure: 1 duration: 6

start: 15 midiStart: 600 pitch: 64 measure: 1 duration: 3
start: 15 midiStart: 600 pitch: 67 measure: 1 duration: 3
start: 15 midiStart: 600 pitch: 72 measure: 1 duration: 3
start: 12 midiStart: 480 pitch: 55 measure: 1 duration: 6

start: 18 midiStart: 720 pitch: 60 measure: 1 duration: 6

start: 21 midiStart: 840 pitch: 64 measure: 1 duration: 3
start: 21 midiStart: 840 pitch: 67 measure: 1 duration: 3
start: 21 midiStart: 840 pitch: 72 measure: 1 duration: 3
start: 18 midiStart: 720 pitch: 60 measure: 1 duration: 6

start: 24 midiStart: 960 pitch: 55 measure: 1 duration: 6

start: 27 midiStart: 1080 pitch: 64 measure: 1 duration: 3
start: 27 midiStart: 1080 pitch: 67 measure: 1 duration: 3
start: 27 midiStart: 1080 pitch: 72 measure: 1 duration: 3
start: 24 midiStart: 960 pitch: 55 measure: 1 duration: 6

start: 30 midiStart: 1200 pitch: 52 measure: 1 duration: 6

start: 33 midiStart: 1320 pitch: 64 measure: 1 duration: 3
start: 33 midiStart: 1320 pitch: 67 measure: 1 duration: 3
start: 33 midiStart: 1320 pitch: 72 measure: 1 duration: 3
start: 30 midiStart: 1200 pitch: 52 measure: 1 duration: 6

start: 36 midiStart: 1440 pitch: 55 measure: 1 duration: 6

start: 39 midiStart: 1560 pitch: 64 measure: 1 duration: 3
start: 39 midiStart: 1560 pitch: 67 measure: 1 duration: 3
start: 39 midiStart: 1560 pitch: 72 measure: 1 duration: 3
start: 36 midiStart: 1440 pitch: 55 measure: 1 duration: 6

start: 42 midiStart: 1680 pitch: 52 measure: 1 duration: 6

start: 45 midiStart: 1800 pitch: 64 measure: 1 duration: 3
start: 45 midiStart: 1800 pitch: 67 measure: 1 duration: 3
start: 45 midiStart: 1800 pitch: 72 measure: 1 duration: 3
start: 42 midiStart: 1680 pitch: 52 measure: 1 duration: 6

start: 48 midiStart: 1920 pitch: 48 measure: 2 duration: 6

start: 51 midiStart: 2040 pitch: 64 measure: 2 duration: 3
start: 51 midiStart: 2040 pitch: 67 measure: 2 duration: 3
start: 51 midiStart: 2040 pitch: 72 measure: 2 duration: 3
start: 48 midiStart: 1920 pitch: 48 measure: 2 duration: 6

start: 54 midiStart: 2160 pitch: 52 measure: 2 duration: 6

start: 57 midiStart: 2280 pitch: 64 measure: 2 duration: 3
start: 57 midiStart: 2280 pitch: 67 measure: 2 duration: 3
start: 57 midiStart: 2280 pitch: 72 measure: 2 duration: 3
start: 54 midiStart: 2160 pitch: 52 measure: 2 duration: 6

start: 60 midiStart: 2400 pitch: 55 measure: 2 duration: 6

start: 63 midiStart: 2520 pitch: 64 measure: 2 duration: 3
start: 63 midiStart: 2520 pitch: 67 measure: 2 duration: 3
start: 63 midiStart: 2520 pitch: 72 measure: 2 duration: 3
start: 60 midiStart: 2400 pitch: 55 measure: 2 duration: 6

start: 66 midiStart: 2640 pitch: 60 measure: 2 duration: 6

start: 69 midiStart: 2760 pitch: 64 measure: 2 duration: 3
start: 69 midiStart: 2760 pitch: 67 measure: 2 duration: 3
start: 69 midiStart: 2760 pitch: 72 measure: 2 duration: 3
start: 66 midiStart: 2640 pitch: 60 measure: 2 duration: 6

start: 72 midiStart: 2880 pitch: 55 measure: 2 duration: 6

start: 75 midiStart: 3000 pitch: 64 measure: 2 duration: 3
start: 75 midiStart: 3000 pitch: 67 measure: 2 duration: 3
start: 75 midiStart: 3000 pitch: 72 measure: 2 duration: 3
start: 72 midiStart: 2880 pitch: 55 measure: 2 duration: 6

start: 78 midiStart: 3120 pitch: 52 measure: 2 duration: 6

start: 81 midiStart: 3240 pitch: 64 measure: 2 duration: 3
start: 81 midiStart: 3240 pitch: 67 measure: 2 duration: 3
start: 81 midiStart: 3240 pitch: 72 measure: 2 duration: 3
start: 78 midiStart: 3120 pitch: 52 measure: 2 duration: 6

start: 84 midiStart: 3360 pitch: 55 measure: 2 duration: 6

start: 87 midiStart: 3480 pitch: 64 measure: 2 duration: 3
start: 87 midiStart: 3480 pitch: 67 measure: 2 duration: 3
start: 87 midiStart: 3480 pitch: 72 measure: 2 duration: 3
start: 84 midiStart: 3360 pitch: 55 measure: 2 duration: 6

start: 90 midiStart: 3600 pitch: 52 measure: 2 duration: 6

start: 93 midiStart: 3720 pitch: 64 measure: 2 duration: 3
start: 93 midiStart: 3720 pitch: 67 measure: 2 duration: 3
start: 93 midiStart: 3720 pitch: 72 measure: 2 duration: 3
start: 90 midiStart: 3600 pitch: 52 measure: 2 duration: 6

start: 96 midiStart: 3840 pitch: 48 measure: 3 duration: 6

start: 99 midiStart: 3960 pitch: 65 measure: 3 duration: 3
start: 99 midiStart: 3960 pitch: 69 measure: 3 duration: 3
start: 99 midiStart: 3960 pitch: 72 measure: 3 duration: 3
start: 96 midiStart: 3840 pitch: 48 measure: 3 duration: 6

start: 102 midiStart: 4080 pitch: 53 measure: 3 duration: 6

start: 105 midiStart: 4200 pitch: 65 measure: 3 duration: 3
start: 105 midiStart: 4200 pitch: 69 measure: 3 duration: 3
start: 105 midiStart: 4200 pitch: 72 measure: 3 duration: 3
start: 102 midiStart: 4080 pitch: 53 measure: 3 duration: 6

start: 108 midiStart: 4320 pitch: 57 measure: 3 duration: 6

start: 111 midiStart: 4440 pitch: 65 measure: 3 duration: 3
start: 111 midiStart: 4440 pitch: 69 measure: 3 duration: 3
start: 111 midiStart: 4440 pitch: 72 measure: 3 duration: 3
start: 108 midiStart: 4320 pitch: 57 measure: 3 duration: 6

start: 114 midiStart: 4560 pitch: 60 measure: 3 duration: 6

start: 117 midiStart: 4680 pitch: 65 measure: 3 duration: 3
start: 117 midiStart: 4680 pitch: 69 measure: 3 duration: 3
start: 117 midiStart: 4680 pitch: 72 measure: 3 duration: 3
start: 114 midiStart: 4560 pitch: 60 measure: 3 duration: 6

start: 120 midiStart: 4800 pitch: 57 measure: 3 duration: 6

start: 123 midiStart: 4920 pitch: 65 measure: 3 duration: 3
start: 123 midiStart: 4920 pitch: 69 measure: 3 duration: 3
start: 123 midiStart: 4920 pitch: 72 measure: 3 duration: 3
start: 120 midiStart: 4800 pitch: 57 measure: 3 duration: 6

start: 126 midiStart: 5040 pitch: 53 measure: 3 duration: 6

start: 129 midiStart: 5160 pitch: 65 measure: 3 duration: 3
start: 129 midiStart: 5160 pitch: 69 measure: 3 duration: 3
start: 129 midiStart: 5160 pitch: 72 measure: 3 duration: 3
start: 126 midiStart: 5040 pitch: 53 measure: 3 duration: 6

start: 132 midiStart: 5280 pitch: 57 measure: 3 duration: 6

start: 135 midiStart: 5400 pitch: 65 measure: 3 duration: 3
start: 135 midiStart: 5400 pitch: 69 measure: 3 duration: 3
start: 135 midiStart: 5400 pitch: 72 measure: 3 duration: 3
start: 132 midiStart: 5280 pitch: 57 measure: 3 duration: 6

start: 138 midiStart: 5520 pitch: 53 measure: 3 duration: 6

start: 141 midiStart: 5640 pitch: 65 measure: 3 duration: 3
start: 141 midiStart: 5640 pitch: 69 measure: 3 duration: 3
start: 141 midiStart: 5640 pitch: 72 measure: 3 duration: 3
start: 138 midiStart: 5520 pitch: 53 measure: 3 duration: 6

start: 144 midiStart: 5760 pitch: 48 measure: 4 duration: 6

start: 147 midiStart: 5880 pitch: 65 measure: 4 duration: 3
start: 147 midiStart: 5880 pitch: 69 measure: 4 duration: 3
start: 147 midiStart: 5880 pitch: 72 measure: 4 duration: 3
start: 144 midiStart: 5760 pitch: 48 measure: 4 duration: 6

start: 150 midiStart: 6000 pitch: 53 measure: 4 duration: 6

start: 153 midiStart: 6120 pitch: 65 measure: 4 duration: 3
start: 153 midiStart: 6120 pitch: 69 measure: 4 duration: 3
start: 153 midiStart: 6120 pitch: 72 measure: 4 duration: 3
start: 150 midiStart: 6000 pitch: 53 measure: 4 duration: 6

start: 156 midiStart: 6240 pitch: 57 measure: 4 duration: 6

start: 159 midiStart: 6360 pitch: 65 measure: 4 duration: 3
start: 159 midiStart: 6360 pitch: 69 measure: 4 duration: 3
start: 159 midiStart: 6360 pitch: 72 measure: 4 duration: 3
start: 156 midiStart: 6240 pitch: 57 measure: 4 duration: 6

start: 162 midiStart: 6480 pitch: 60 measure: 4 duration: 6

start: 165 midiStart: 6600 pitch: 65 measure: 4 duration: 3
start: 165 midiStart: 6600 pitch: 69 measure: 4 duration: 3
start: 165 midiStart: 6600 pitch: 72 measure: 4 duration: 3
start: 162 midiStart: 6480 pitch: 60 measure: 4 duration: 6

start: 168 midiStart: 6720 pitch: 57 measure: 4 duration: 6

start: 171 midiStart: 6840 pitch: 65 measure: 4 duration: 3
start: 171 midiStart: 6840 pitch: 69 measure: 4 duration: 3
start: 171 midiStart: 6840 pitch: 72 measure: 4 duration: 3
start: 168 midiStart: 6720 pitch: 57 measure: 4 duration: 6

start: 174 midiStart: 6960 pitch: 53 measure: 4 duration: 6

start: 177 midiStart: 7080 pitch: 65 measure: 4 duration: 3
start: 177 midiStart: 7080 pitch: 69 measure: 4 duration: 3
start: 177 midiStart: 7080 pitch: 72 measure: 4 duration: 3
start: 174 midiStart: 6960 pitch: 53 measure: 4 duration: 6

start: 180 midiStart: 7200 pitch: 57 measure: 4 duration: 6

start: 183 midiStart: 7320 pitch: 65 measure: 4 duration: 3
start: 183 midiStart: 7320 pitch: 69 measure: 4 duration: 3
start: 183 midiStart: 7320 pitch: 72 measure: 4 duration: 3
start: 180 midiStart: 7200 pitch: 57 measure: 4 duration: 6

start: 186 midiStart: 7440 pitch: 53 measure: 4 duration: 6

start: 189 midiStart: 7560 pitch: 65 measure: 4 duration: 3
start: 189 midiStart: 7560 pitch: 69 measure: 4 duration: 3
start: 189 midiStart: 7560 pitch: 72 measure: 4 duration: 3
start: 186 midiStart: 7440 pitch: 53 measure: 4 duration: 6

start: 192 midiStart: 7680 pitch: 48 measure: 5 duration: 6

start: 195 midiStart: 7800 pitch: 67 measure: 5 duration: 3
start: 195 midiStart: 7800 pitch: 71 measure: 5 duration: 3
start: 195 midiStart: 7800 pitch: 72 measure: 5 duration: 3
start: 192 midiStart: 7680 pitch: 48 measure: 5 duration: 6

start: 198 midiStart: 7920 pitch: 55 measure: 5 duration: 6

start: 201 midiStart: 8040 pitch: 67 measure: 5 duration: 3
start: 201 midiStart: 8040 pitch: 71 measure: 5 duration: 3
start: 201 midiStart: 8040 pitch: 72 measure: 5 duration: 3
start: 198 midiStart: 7920 pitch: 55 measure: 5 duration: 6

start: 204 midiStart: 8160 pitch: 59 measure: 5 duration: 6

start: 207 midiStart: 8280 pitch: 67 measure: 5 duration: 3
start: 207 midiStart: 8280 pitch: 71 measure: 5 duration: 3
start: 207 midiStart: 8280 pitch: 72 measure: 5 duration: 3
start: 204 midiStart: 8160 pitch: 59 measure: 5 duration: 6

start: 210 midiStart: 8400 pitch: 60 measure: 5 duration: 6

start: 213 midiStart: 8520 pitch: 67 measure: 5 duration: 3
start: 213 midiStart: 8520 pitch: 71 measure: 5 duration: 3
start: 213 midiStart: 8520 pitch: 72 measure: 5 duration: 3
start: 210 midiStart: 8400 pitch: 60 measure: 5 duration: 6

start: 216 midiStart: 8640 pitch: 59 measure: 5 duration: 6

start: 219 midiStart: 8760 pitch: 67 measure: 5 duration: 3
start: 219 midiStart: 8760 pitch: 71 measure: 5 duration: 3
start: 219 midiStart: 8760 pitch: 72 measure: 5 duration: 3
start: 216 midiStart: 8640 pitch: 59 measure: 5 duration: 6

start: 222 midiStart: 8880 pitch: 55 measure: 5 duration: 6

start: 225 midiStart: 9000 pitch: 67 measure: 5 duration: 3
start: 225 midiStart: 9000 pitch: 71 measure: 5 duration: 3
start: 225 midiStart: 9000 pitch: 72 measure: 5 duration: 3
start: 222 midiStart: 8880 pitch: 55 measure: 5 duration: 6

start: 228 midiStart: 9120 pitch: 59 measure: 5 duration: 6

start: 231 midiStart: 9240 pitch: 67 measure: 5 duration: 3
start: 231 midiStart: 9240 pitch: 71 measure: 5 duration: 3
start: 231 midiStart: 9240 pitch: 72 measure: 5 duration: 3
start: 228 midiStart: 9120 pitch: 59 measure: 5 duration: 6

start: 234 midiStart: 9360 pitch: 55 measure: 5 duration: 6

start: 237 midiStart: 9480 pitch: 67 measure: 5 duration: 3
start: 237 midiStart: 9480 pitch: 71 measure: 5 duration: 3
start: 237 midiStart: 9480 pitch: 72 measure: 5 duration: 3
start: 234 midiStart: 9360 pitch: 55 measure: 5 duration: 6

start: 240 midiStart: 9600 pitch: 48 measure: 6 duration: 6

start: 243 midiStart: 9720 pitch: 67 measure: 6 duration: 3
start: 243 midiStart: 9720 pitch: 71 measure: 6 duration: 3
start: 243 midiStart: 9720 pitch: 72 measure: 6 duration: 3
start: 240 midiStart: 9600 pitch: 48 measure: 6 duration: 6

start: 246 midiStart: 9840 pitch: 55 measure: 6 duration: 6

start: 249 midiStart: 9960 pitch: 67 measure: 6 duration: 3
start: 249 midiStart: 9960 pitch: 71 measure: 6 duration: 3
start: 249 midiStart: 9960 pitch: 72 measure: 6 duration: 3
start: 246 midiStart: 9840 pitch: 55 measure: 6 duration: 6

start: 252 midiStart: 10080 pitch: 59 measure: 6 duration: 6

start: 255 midiStart: 10200 pitch: 67 measure: 6 duration: 3
start: 255 midiStart: 10200 pitch: 71 measure: 6 duration: 3
start: 255 midiStart: 10200 pitch: 72 measure: 6 duration: 3
start: 252 midiStart: 10080 pitch: 59 measure: 6 duration: 6

start: 258 midiStart: 10320 pitch: 60 measure: 6 duration: 6

start: 261 midiStart: 10440 pitch: 67 measure: 6 duration: 3
start: 261 midiStart: 10440 pitch: 71 measure: 6 duration: 3
start: 261 midiStart: 10440 pitch: 72 measure: 6 duration: 3
start: 258 midiStart: 10320 pitch: 60 measure: 6 duration: 6

start: 264 midiStart: 10560 pitch: 59 measure: 6 duration: 6

start: 267 midiStart: 10680 pitch: 67 measure: 6 duration: 3
start: 267 midiStart: 10680 pitch: 71 measure: 6 duration: 3
start: 267 midiStart: 10680 pitch: 72 measure: 6 duration: 3
start: 264 midiStart: 10560 pitch: 59 measure: 6 duration: 6

start: 270 midiStart: 10800 pitch: 55 measure: 6 duration: 6

start: 273 midiStart: 10920 pitch: 67 measure: 6 duration: 3
start: 273 midiStart: 10920 pitch: 71 measure: 6 duration: 3
start: 273 midiStart: 10920 pitch: 72 measure: 6 duration: 3
start: 270 midiStart: 10800 pitch: 55 measure: 6 duration: 6

start: 276 midiStart: 11040 pitch: 59 measure: 6 duration: 6

start: 279 midiStart: 11160 pitch: 67 measure: 6 duration: 3
start: 279 midiStart: 11160 pitch: 71 measure: 6 duration: 3
start: 279 midiStart: 11160 pitch: 72 measure: 6 duration: 3
start: 276 midiStart: 11040 pitch: 59 measure: 6 duration: 6

start: 282 midiStart: 11280 pitch: 55 measure: 6 duration: 6

start: 285 midiStart: 11400 pitch: 67 measure: 6 duration: 3
start: 285 midiStart: 11400 pitch: 71 measure: 6 duration: 3
start: 285 midiStart: 11400 pitch: 72 measure: 6 duration: 3
start: 282 midiStart: 11280 pitch: 55 measure: 6 duration: 6

start: 288 midiStart: 11520 pitch: 48 measure: 7 duration: 6

start: 291 midiStart: 11640 pitch: 64 measure: 7 duration: 3
start: 291 midiStart: 11640 pitch: 67 measure: 7 duration: 3
start: 291 midiStart: 11640 pitch: 72 measure: 7 duration: 3
start: 288 midiStart: 11520 pitch: 48 measure: 7 duration: 6

start: 294 midiStart: 11760 pitch: 55 measure: 7 duration: 6

start: 297 midiStart: 11880 pitch: 64 measure: 7 duration: 3
start: 297 midiStart: 11880 pitch: 67 measure: 7 duration: 3
start: 297 midiStart: 11880 pitch: 72 measure: 7 duration: 3
start: 294 midiStart: 11760 pitch: 55 measure: 7 duration: 6

start: 300 midiStart: 12000 pitch: 60 measure: 7 duration: 6

start: 303 midiStart: 12120 pitch: 64 measure: 7 duration: 3
start: 303 midiStart: 12120 pitch: 67 measure: 7 duration: 3
start: 303 midiStart: 12120 pitch: 72 measure: 7 duration: 3
start: 300 midiStart: 12000 pitch: 60 measure: 7 duration: 6

start: 306 midiStart: 12240 pitch: 63 measure: 7 duration: 6

start: 309 midiStart: 12360 pitch: 64 measure: 7 duration: 3
start: 309 midiStart: 12360 pitch: 67 measure: 7 duration: 3
start: 309 midiStart: 12360 pitch: 72 measure: 7 duration: 3
start: 306 midiStart: 12240 pitch: 63 measure: 7 duration: 6

start: 312 midiStart: 12480 pitch: 60 measure: 7 duration: 6

start: 315 midiStart: 12600 pitch: 64 measure: 7 duration: 3
start: 315 midiStart: 12600 pitch: 67 measure: 7 duration: 3
start: 315 midiStart: 12600 pitch: 72 measure: 7 duration: 3
start: 312 midiStart: 12480 pitch: 60 measure: 7 duration: 6

start: 318 midiStart: 12720 pitch: 58 measure: 7 duration: 6

start: 321 midiStart: 12840 pitch: 64 measure: 7 duration: 3
start: 321 midiStart: 12840 pitch: 67 measure: 7 duration: 3
start: 321 midiStart: 12840 pitch: 72 measure: 7 duration: 3
start: 318 midiStart: 12720 pitch: 58 measure: 7 duration: 6

start: 324 midiStart: 12960 pitch: 60 measure: 7 duration: 6

start: 327 midiStart: 13080 pitch: 64 measure: 7 duration: 3
start: 327 midiStart: 13080 pitch: 67 measure: 7 duration: 3
start: 327 midiStart: 13080 pitch: 72 measure: 7 duration: 3
start: 324 midiStart: 12960 pitch: 60 measure: 7 duration: 6

start: 330 midiStart: 13200 pitch: 58 measure: 7 duration: 6

start: 333 midiStart: 13320 pitch: 64 measure: 7 duration: 3
start: 333 midiStart: 13320 pitch: 67 measure: 7 duration: 3
start: 333 midiStart: 13320 pitch: 72 measure: 7 duration: 3
start: 330 midiStart: 13200 pitch: 58 measure: 7 duration: 6

start: 336 midiStart: 13440 pitch: 55 measure: 8 duration: 6

start: 339 midiStart: 13560 pitch: 64 measure: 8 duration: 3
start: 339 midiStart: 13560 pitch: 67 measure: 8 duration: 3
start: 339 midiStart: 13560 pitch: 72 measure: 8 duration: 3
start: 336 midiStart: 13440 pitch: 55 measure: 8 duration: 6

start: 342 midiStart: 13680 pitch: 53 measure: 8 duration: 6

start: 345 midiStart: 13800 pitch: 64 measure: 8 duration: 3
start: 345 midiStart: 13800 pitch: 67 measure: 8 duration: 3
start: 345 midiStart: 13800 pitch: 72 measure: 8 duration: 3
start: 342 midiStart: 13680 pitch: 53 measure: 8 duration: 6

start: 348 midiStart: 13920 pitch: 55 measure: 8 duration: 6

start: 351 midiStart: 14040 pitch: 64 measure: 8 duration: 3
start: 351 midiStart: 14040 pitch: 67 measure: 8 duration: 3
start: 351 midiStart: 14040 pitch: 72 measure: 8 duration: 3
start: 348 midiStart: 13920 pitch: 55 measure: 8 duration: 6

start: 354 midiStart: 14160 pitch: 53 measure: 8 duration: 6

start: 357 midiStart: 14280 pitch: 64 measure: 8 duration: 3
start: 357 midiStart: 14280 pitch: 67 measure: 8 duration: 3
start: 357 midiStart: 14280 pitch: 72 measure: 8 duration: 3
start: 354 midiStart: 14160 pitch: 53 measure: 8 duration: 6

start: 360 midiStart: 14400 pitch: 51 measure: 8 duration: 6

start: 363 midiStart: 14520 pitch: 64 measure: 8 duration: 3
start: 363 midiStart: 14520 pitch: 67 measure: 8 duration: 3
start: 363 midiStart: 14520 pitch: 72 measure: 8 duration: 3
start: 360 midiStart: 14400 pitch: 51 measure: 8 duration: 6

start: 366 midiStart: 14640 pitch: 48 measure: 8 duration: 6

start: 369 midiStart: 14760 pitch: 64 measure: 8 duration: 3
start: 369 midiStart: 14760 pitch: 67 measure: 8 duration: 3
start: 369 midiStart: 14760 pitch: 72 measure: 8 duration: 3
start: 366 midiStart: 14640 pitch: 48 measure: 8 duration: 6

start: 372 midiStart: 14880 pitch: 46 measure: 8 duration: 6

start: 375 midiStart: 15000 pitch: 64 measure: 8 duration: 3
start: 375 midiStart: 15000 pitch: 67 measure: 8 duration: 3
start: 375 midiStart: 15000 pitch: 72 measure: 8 duration: 3
start: 372 midiStart: 14880 pitch: 46 measure: 8 duration: 6

start: 378 midiStart: 15120 pitch: 43 measure: 8 duration: 6

start: 381 midiStart: 15240 pitch: 64 measure: 8 duration: 3
start: 381 midiStart: 15240 pitch: 67 measure: 8 duration: 3
start: 381 midiStart: 15240 pitch: 72 measure: 8 duration: 3
start: 378 midiStart: 15120 pitch: 43 measure: 8 duration: 6

start: 384 midiStart: 15360 pitch: 41 measure: 9 duration: 6

start: 387 midiStart: 15480 pitch: 65 measure: 9 duration: 3
start: 387 midiStart: 15480 pitch: 69 measure: 9 duration: 3
start: 387 midiStart: 15480 pitch: 72 measure: 9 duration: 3
start: 384 midiStart: 15360 pitch: 41 measure: 9 duration: 6

start: 390 midiStart: 15600 pitch: 45 measure: 9 duration: 6

start: 393 midiStart: 15720 pitch: 65 measure: 9 duration: 3
start: 393 midiStart: 15720 pitch: 69 measure: 9 duration: 3
start: 393 midiStart: 15720 pitch: 72 measure: 9 duration: 3
start: 390 midiStart: 15600 pitch: 45 measure: 9 duration: 6

start: 396 midiStart: 15840 pitch: 48 measure: 9 duration: 6

start: 399 midiStart: 15960 pitch: 65 measure: 9 duration: 3
start: 399 midiStart: 15960 pitch: 69 measure: 9 duration: 3
start: 399 midiStart: 15960 pitch: 72 measure: 9 duration: 3
start: 396 midiStart: 15840 pitch: 48 measure: 9 duration: 6

start: 402 midiStart: 16080 pitch: 53 measure: 9 duration: 6

start: 405 midiStart: 16200 pitch: 65 measure: 9 duration: 3
start: 405 midiStart: 16200 pitch: 69 measure: 9 duration: 3
start: 405 midiStart: 16200 pitch: 72 measure: 9 duration: 3
start: 402 midiStart: 16080 pitch: 53 measure: 9 duration: 6

start: 408 midiStart: 16320 pitch: 48 measure: 9 duration: 6

start: 411 midiStart: 16440 pitch: 65 measure: 9 duration: 3
start: 411 midiStart: 16440 pitch: 69 measure: 9 duration: 3
start: 411 midiStart: 16440 pitch: 72 measure: 9 duration: 3
start: 408 midiStart: 16320 pitch: 48 measure: 9 duration: 6

start: 414 midiStart: 16560 pitch: 45 measure: 9 duration: 6

start: 417 midiStart: 16680 pitch: 65 measure: 9 duration: 3
start: 417 midiStart: 16680 pitch: 69 measure: 9 duration: 3
start: 417 midiStart: 16680 pitch: 72 measure: 9 duration: 3
start: 414 midiStart: 16560 pitch: 45 measure: 9 duration: 6

start: 420 midiStart: 16800 pitch: 48 measure: 9 duration: 6

start: 423 midiStart: 16920 pitch: 65 measure: 9 duration: 3
start: 423 midiStart: 16920 pitch: 69 measure: 9 duration: 3
start: 423 midiStart: 16920 pitch: 72 measure: 9 duration: 3
start: 420 midiStart: 16800 pitch: 48 measure: 9 duration: 6

start: 426 midiStart: 17040 pitch: 45 measure: 9 duration: 6

start: 429 midiStart: 17160 pitch: 65 measure: 9 duration: 3
start: 429 midiStart: 17160 pitch: 69 measure: 9 duration: 3
start: 429 midiStart: 17160 pitch: 72 measure: 9 duration: 3
start: 426 midiStart: 17040 pitch: 45 measure: 9 duration: 6

start: 432 midiStart: 17280 pitch: 41 measure: 10 duration: 6

start: 435 midiStart: 17400 pitch: 65 measure: 10 duration: 3
start: 435 midiStart: 17400 pitch: 69 measure: 10 duration: 3
start: 435 midiStart: 17400 pitch: 72 measure: 10 duration: 3
start: 432 midiStart: 17280 pitch: 41 measure: 10 duration: 6

start: 438 midiStart: 17520 pitch: 45 measure: 10 duration: 6

start: 441 midiStart: 17640 pitch: 65 measure: 10 duration: 3
start: 441 midiStart: 17640 pitch: 69 measure: 10 duration: 3
start: 441 midiStart: 17640 pitch: 72 measure: 10 duration: 3
start: 438 midiStart: 17520 pitch: 45 measure: 10 duration: 6

start: 444 midiStart: 17760 pitch: 48 measure: 10 duration: 6

start: 447 midiStart: 17880 pitch: 65 measure: 10 duration: 3
start: 447 midiStart: 17880 pitch: 69 measure: 10 duration: 3
start: 447 midiStart: 17880 pitch: 72 measure: 10 duration: 3
start: 444 midiStart: 17760 pitch: 48 measure: 10 duration: 6

start: 450 midiStart: 18000 pitch: 53 measure: 10 duration: 6

start: 453 midiStart: 18120 pitch: 65 measure: 10 duration: 3
start: 453 midiStart: 18120 pitch: 69 measure: 10 duration: 3
start: 453 midiStart: 18120 pitch: 72 measure: 10 duration: 3
start: 450 midiStart: 18000 pitch: 53 measure: 10 duration: 6

start: 456 midiStart: 18240 pitch: 48 measure: 10 duration: 6

start: 459 midiStart: 18360 pitch: 65 measure: 10 duration: 3
start: 459 midiStart: 18360 pitch: 69 measure: 10 duration: 3
start: 459 midiStart: 18360 pitch: 72 measure: 10 duration: 3
start: 456 midiStart: 18240 pitch: 48 measure: 10 duration: 6

start: 462 midiStart: 18480 pitch: 45 measure: 10 duration: 6

start: 465 midiStart: 18600 pitch: 65 measure: 10 duration: 3
start: 465 midiStart: 18600 pitch: 69 measure: 10 duration: 3
start: 465 midiStart: 18600 pitch: 72 measure: 10 duration: 3
start: 462 midiStart: 18480 pitch: 45 measure: 10 duration: 6

start: 468 midiStart: 18720 pitch: 48 measure: 10 duration: 6

start: 471 midiStart: 18840 pitch: 65 measure: 10 duration: 3
start: 471 midiStart: 18840 pitch: 69 measure: 10 duration: 3
start: 471 midiStart: 18840 pitch: 72 measure: 10 duration: 3
start: 468 midiStart: 18720 pitch: 48 measure: 10 duration: 6

start: 474 midiStart: 18960 pitch: 45 measure: 10 duration: 6

start: 477 midiStart: 19080 pitch: 65 measure: 10 duration: 3
start: 477 midiStart: 19080 pitch: 69 measure: 10 duration: 3
start: 477 midiStart: 19080 pitch: 72 measure: 10 duration: 3
start: 474 midiStart: 18960 pitch: 45 measure: 10 duration: 6

start: 480 midiStart: 19200 pitch: 43 measure: 11 duration: 6

start: 483 midiStart: 19320 pitch: 67 measure: 11 duration: 3
start: 483 midiStart: 19320 pitch: 71 measure: 11 duration: 3
start: 483 midiStart: 19320 pitch: 74 measure: 11 duration: 3
start: 480 midiStart: 19200 pitch: 43 measure: 11 duration: 6

start: 486 midiStart: 19440 pitch: 47 measure: 11 duration: 6

start: 489 midiStart: 19560 pitch: 67 measure: 11 duration: 3
start: 489 midiStart: 19560 pitch: 71 measure: 11 duration: 3
start: 489 midiStart: 19560 pitch: 74 measure: 11 duration: 3
start: 486 midiStart: 19440 pitch: 47 measure: 11 duration: 6

start: 492 midiStart: 19680 pitch: 50 measure: 11 duration: 6

start: 495 midiStart: 19800 pitch: 67 measure: 11 duration: 3
start: 495 midiStart: 19800 pitch: 71 measure: 11 duration: 3
start: 495 midiStart: 19800 pitch: 74 measure: 11 duration: 3
start: 492 midiStart: 19680 pitch: 50 measure: 11 duration: 6

start: 498 midiStart: 19920 pitch: 55 measure: 11 duration: 6

start: 501 midiStart: 20040 pitch: 67 measure: 11 duration: 3
start: 501 midiStart: 20040 pitch: 71 measure: 11 duration: 3
start: 501 midiStart: 20040 pitch: 74 measure: 11 duration: 3
start: 498 midiStart: 19920 pitch: 55 measure: 11 duration: 6

start: 504 midiStart: 20160 pitch: 50 measure: 11 duration: 6

start: 507 midiStart: 20280 pitch: 67 measure: 11 duration: 3
start: 507 midiStart: 20280 pitch: 71 measure: 11 duration: 3
start: 507 midiStart: 20280 pitch: 74 measure: 11 duration: 3
start: 504 midiStart: 20160 pitch: 50 measure: 11 duration: 6

start: 510 midiStart: 20400 pitch: 47 measure: 11 duration: 6

start: 513 midiStart: 20520 pitch: 67 measure: 11 duration: 3
start: 513 midiStart: 20520 pitch: 71 measure: 11 duration: 3
start: 513 midiStart: 20520 pitch: 74 measure: 11 duration: 3
start: 510 midiStart: 20400 pitch: 47 measure: 11 duration: 6

start: 516 midiStart: 20640 pitch: 50 measure: 11 duration: 6

start: 519 midiStart: 20760 pitch: 67 measure: 11 duration: 3
start: 519 midiStart: 20760 pitch: 71 measure: 11 duration: 3
start: 519 midiStart: 20760 pitch: 74 measure: 11 duration: 3
start: 516 midiStart: 20640 pitch: 50 measure: 11 duration: 6

start: 522 midiStart: 20880 pitch: 47 measure: 11 duration: 6

start: 525 midiStart: 21000 pitch: 67 measure: 11 duration: 3
start: 525 midiStart: 21000 pitch: 71 measure: 11 duration: 3
start: 525 midiStart: 21000 pitch: 74 measure: 11 duration: 3
start: 522 midiStart: 20880 pitch: 47 measure: 11 duration: 6

start: 528 midiStart: 21120 pitch: 43 measure: 12 duration: 6

start: 531 midiStart: 21240 pitch: 67 measure: 12 duration: 3
start: 531 midiStart: 21240 pitch: 71 measure: 12 duration: 3
start: 531 midiStart: 21240 pitch: 74 measure: 12 duration: 3
start: 528 midiStart: 21120 pitch: 43 measure: 12 duration: 6

start: 534 midiStart: 21360 pitch: 47 measure: 12 duration: 6

start: 537 midiStart: 21480 pitch: 67 measure: 12 duration: 3
start: 537 midiStart: 21480 pitch: 71 measure: 12 duration: 3
start: 537 midiStart: 21480 pitch: 74 measure: 12 duration: 3
start: 534 midiStart: 21360 pitch: 47 measure: 12 duration: 6

start: 540 midiStart: 21600 pitch: 50 measure: 12 duration: 6

start: 543 midiStart: 21720 pitch: 67 measure: 12 duration: 3
start: 543 midiStart: 21720 pitch: 71 measure: 12 duration: 3
start: 543 midiStart: 21720 pitch: 74 measure: 12 duration: 3
start: 540 midiStart: 21600 pitch: 50 measure: 12 duration: 6

start: 546 midiStart: 21840 pitch: 52 measure: 12 duration: 6

start: 549 midiStart: 21960 pitch: 67 measure: 12 duration: 3
start: 549 midiStart: 21960 pitch: 71 measure: 12 duration: 3
start: 549 midiStart: 21960 pitch: 74 measure: 12 duration: 3
start: 546 midiStart: 21840 pitch: 52 measure: 12 duration: 6

start: 552 midiStart: 22080 pitch: 53 measure: 12 duration: 6

start: 555 midiStart: 22200 pitch: 67 measure: 12 duration: 3
start: 555 midiStart: 22200 pitch: 71 measure: 12 duration: 3
start: 555 midiStart: 22200 pitch: 74 measure: 12 duration: 3
start: 552 midiStart: 22080 pitch: 53 measure: 12 duration: 6

start: 558 midiStart: 22320 pitch: 52 measure: 12 duration: 6

start: 561 midiStart: 22440 pitch: 67 measure: 12 duration: 3
start: 561 midiStart: 22440 pitch: 71 measure: 12 duration: 3
start: 561 midiStart: 22440 pitch: 74 measure: 12 duration: 3
start: 558 midiStart: 22320 pitch: 52 measure: 12 duration: 6

start: 564 midiStart: 22560 pitch: 50 measure: 12 duration: 6

start: 567 midiStart: 22680 pitch: 67 measure: 12 duration: 3
start: 567 midiStart: 22680 pitch: 71 measure: 12 duration: 3
start: 567 midiStart: 22680 pitch: 74 measure: 12 duration: 3
start: 564 midiStart: 22560 pitch: 50 measure: 12 duration: 6

start: 570 midiStart: 22800 pitch: 47 measure: 12 duration: 6

start: 573 midiStart: 22920 pitch: 67 measure: 12 duration: 3
start: 573 midiStart: 22920 pitch: 71 measure: 12 duration: 3
start: 573 midiStart: 22920 pitch: 74 measure: 12 duration: 3
start: 570 midiStart: 22800 pitch: 47 measure: 12 duration: 6

start: 576 midiStart: 23040 pitch: 48 measure: 13 duration: 6

start: 579 midiStart: 23160 pitch: 67 measure: 13 duration: 3
start: 579 midiStart: 23160 pitch: 72 measure: 13 duration: 3
start: 579 midiStart: 23160 pitch: 76 measure: 13 duration: 3
start: 576 midiStart: 23040 pitch: 48 measure: 13 duration: 6

start: 582 midiStart: 23280 pitch: 52 measure: 13 duration: 6

start: 585 midiStart: 23400 pitch: 67 measure: 13 duration: 3
start: 585 midiStart: 23400 pitch: 72 measure: 13 duration: 3
start: 585 midiStart: 23400 pitch: 76 measure: 13 duration: 3
start: 582 midiStart: 23280 pitch: 52 measure: 13 duration: 6

start: 588 midiStart: 23520 pitch: 55 measure: 13 duration: 6

start: 591 midiStart: 23640 pitch: 67 measure: 13 duration: 3
start: 591 midiStart: 23640 pitch: 72 measure: 13 duration: 3
start: 591 midiStart: 23640 pitch: 76 measure: 13 duration: 3
start: 588 midiStart: 23520 pitch: 55 measure: 13 duration: 6

start: 594 midiStart: 23760 pitch: 60 measure: 13 duration: 6

start: 597 midiStart: 23880 pitch: 67 measure: 13 duration: 3
start: 597 midiStart: 23880 pitch: 72 measure: 13 duration: 3
start: 597 midiStart: 23880 pitch: 76 measure: 13 duration: 3
start: 594 midiStart: 23760 pitch: 60 measure: 13 duration: 6

start: 600 midiStart: 24000 pitch: 55 measure: 13 duration: 6

start: 603 midiStart: 24120 pitch: 67 measure: 13 duration: 3
start: 603 midiStart: 24120 pitch: 72 measure: 13 duration: 3
start: 603 midiStart: 24120 pitch: 76 measure: 13 duration: 3
start: 600 midiStart: 24000 pitch: 55 measure: 13 duration: 6

start: 606 midiStart: 24240 pitch: 52 measure: 13 duration: 6

start: 609 midiStart: 24360 pitch: 67 measure: 13 duration: 3
start: 609 midiStart: 24360 pitch: 72 measure: 13 duration: 3
start: 609 midiStart: 24360 pitch: 76 measure: 13 duration: 3
start: 606 midiStart: 24240 pitch: 52 measure: 13 duration: 6

start: 612 midiStart: 24480 pitch: 55 measure: 13 duration: 6

start: 615 midiStart: 24600 pitch: 67 measure: 13 duration: 3
start: 615 midiStart: 24600 pitch: 72 measure: 13 duration: 3
start: 615 midiStart: 24600 pitch: 76 measure: 13 duration: 3
start: 612 midiStart: 24480 pitch: 55 measure: 13 duration: 6

start: 618 midiStart: 24720 pitch: 52 measure: 13 duration: 6

start: 621 midiStart: 24840 pitch: 67 measure: 13 duration: 3
start: 621 midiStart: 24840 pitch: 72 measure: 13 duration: 3
start: 621 midiStart: 24840 pitch: 76 measure: 13 duration: 3
start: 618 midiStart: 24720 pitch: 52 measure: 13 duration: 6

start: 624 midiStart: 24960 pitch: 48 measure: 14 duration: 6

start: 627 midiStart: 25080 pitch: 69 measure: 14 duration: 3
start: 627 midiStart: 25080 pitch: 72 measure: 14 duration: 3
start: 627 midiStart: 25080 pitch: 77 measure: 14 duration: 3
start: 624 midiStart: 24960 pitch: 48 measure: 14 duration: 6

start: 630 midiStart: 25200 pitch: 53 measure: 14 duration: 6

start: 633 midiStart: 25320 pitch: 69 measure: 14 duration: 3
start: 633 midiStart: 25320 pitch: 72 measure: 14 duration: 3
start: 633 midiStart: 25320 pitch: 77 measure: 14 duration: 3
start: 630 midiStart: 25200 pitch: 53 measure: 14 duration: 6

start: 636 midiStart: 25440 pitch: 57 measure: 14 duration: 6

start: 639 midiStart: 25560 pitch: 69 measure: 14 duration: 3
start: 639 midiStart: 25560 pitch: 72 measure: 14 duration: 3
start: 639 midiStart: 25560 pitch: 77 measure: 14 duration: 3
start: 636 midiStart: 25440 pitch: 57 measure: 14 duration: 6

start: 642 midiStart: 25680 pitch: 60 measure: 14 duration: 6

start: 645