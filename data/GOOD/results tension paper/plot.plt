set parametric
unset key
unset tics
unset border 

set style line 4 lc rgb 'black' pt 7
set style line 1 lc rgb 'blue' pt 7 ps 3
set style line 2 lc rgb 'red' pt 7 ps 6
set style line 3 lc rgb 'green' pt 7 ps 8

splot [t=0:3*pi] sin(t),cos(t),t*0.23246067325 ls 4,  '-' w p ls 1,  '-' w p ls 2,  '-' w p ls 3
0.0 1.0 0.0
e
1.0 0.0 0.3651483716701107
e
0.0 -1.0 0.7302967433402214
e


set xrange [-5:5]
show xrange
set yrange [-5:5]
show yrange
